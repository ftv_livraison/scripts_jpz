#!/bin/ksh
# Deploiement des agents
# Les agents configures dans $WSO2_AGENTS sont deployes dans WSO2-EMM
export FPATH=/appli/product/fonctions

init_envir $LINENO
EXIT_NOW=1

for INFO_AGENT in $WSO2_AGENTS 
do
	TYPE_AGENT=$(echo $INFO_AGENT | cut -d: -f1)
	FIC_AGENT=$(echo $INFO_AGENT | cut -d: -f2)
	if [ -f $FTV_DIR_AGENTS/$FIC_AGENT ]
	then
		exec_CMD $LINENO cp $FTV_DIR_AGENTS/$FIC_AGENT $WSO2_HOME/repository/deployment/server/jaggeryapps/emm/client_app/$FIC_AGENT
	else
		msg_echo $LINENO "Aucun agent $TYPE_AGENT a deployer ..."
	fi
	test_cr $LINENO $EXIT 1
done