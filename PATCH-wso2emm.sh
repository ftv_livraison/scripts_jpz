#!/bin/ksh
# Repertoire des fonctions
export FPATH=/appli/product/fonctions
# Intialisation de l'environnement
init_envir $LINENO
EXIT_NOW=1
TAG_HORAIRE=$(date "+%Y%m%d.%H%M%S")

# Recherche de la commande patch
exec_CMD $LINENO type patch > /dev/null
msg_echo $LINENO La commande patch a ete trouvee

# Recuperation des parametres
# Repertoire du ou des patch
REP_PATCH=""
# Nom du ou des fichier(s) du patch
FIC_PATCH=""
# Indique si c'est un patch systeme ou non
PATCH_SYSTEME=0
while [ $# -ne 0 ]
do
	case $1 in
	-s )	PATCH_SYSTEME=1
	;;
	-r* )	REP_PATCH=$(echo $1 | cut -c3-)
	;;	
	* )	[ "$FIC_PATCH" = "" ] && FIC_PATCH=$1 || FIC_PATCH=$FIC_PATCH" "$1
	;;
	esac
	shift
done

# Si aucun repertoire
if [ "$REP_PATCH" = "" ]
then
	msg_echo $LINENO "ERREUR Il manque le repertoire"
	test_cr $LINENO 1
else
	if [ -d $REP_PATCH ]
	then
		msg_echo $LINENO "Le repertoire $REP_PATCH a bien ete trouve"
	else
		msg_echo $LINENO "ERREUR $REP_PATCH n'est pas un repertoire"
		test_cr $LINENO 1
	fi
fi

[ $PATCH_SYSTEME -eq 1 ] && msg_echo $LINENO "PATCH PATCH_SYSTEMEE"

# Si aucun patch 
if [ "$FIC_PATCH" = "" ]
then
	msg_echo $LINENO "ERREUR Il manque le fichier patch a installer"
	test_cr $LINENO 1
else
	# Si ce n'est pas un patch systeme
	if [ $PATCH_SYSTEME -eq 0 ]
	then
		# Nb de fichiers patch recus en parametre
		NB_FIC=0
		# Nb de fichiers patch trouves
		NB_FIC_OK=0
		for tFIC in $FIC_PATCH
		do
			if [ $(echo $tFIC | grep -c ":") -ne 0 ]
			then
				FIC=$(echo $tFIC | cut -d: -f2)
			else
				FIC=$tFIC
			fi
			NB_FIC=$((NB_FIC + 1))
			
			FIC_TEMPLATE=$REP_PATCH/$FIC.src
			if [ -f $FIC_TEMPLATE ]
			then
				msg_echo $LINENO "Un fichier $FIC_TEMPLATE a ete trouve"
				template_2_file $LINENO $FIC_TEMPLATE $REP_PATCH/$FIC
				if [ -f $REP_PATCH/$FIC ]
				then
					msg_echo $LINENO "Le fichier $FIC a bien ete genere"
				else
					msg_echo $LINENO "ERREUR Le fichier $FIC n'a pas pu etre localise"
					msg_echo $LINENO "ERREUR template_2_file"
				fi
			fi
			
			if [ -f $REP_PATCH/$FIC ]
			then
				msg_echo $LINENO "Le fichier $FIC a bien ete trouve"
				NB_FIC_OK=$((NB_FIC_OK + 1))
			else
				msg_echo $LINENO "Le fichier $FIC n'a pas pu etre localise"
				
			fi
		done
		# Si on a pas trouve tous les fichiers : ERREUR
		if [ $NB_FIC -ne $NB_FIC_OK ]
		then
			msg_echo $LINENO "ERREUR Tous les fichiers n'ont pas pu etre trouves ou generes"
			test_cr $LINENO 1
		fi
		msg_echo $LINENO "Tous les fichiers ont ete trouves"
	fi
fi

# Pour chaque fichier
for tFIC in $FIC_PATCH
do
	if [ $(echo $tFIC | grep -c ":") -ne 0 ]
	then
		FIC=$(echo $tFIC | cut -d: -f2)
		# Repertoire demande pour le patch, WSO2_HOME par defaut (car les chemins dans les patchs sont relatifs)
		REP_DEST=$(echo $tFIC | cut -d: -f1)
	else
		FIC=$tFIC
		# WSO2_HOME par defaut (car les chemins dans les patchs sont relatifs)
		REP_DEST=$WSO2_HOME
	fi
	
	# Appliquer la patch
	msg_echo $LINENO "Application du patch $FIC"
	if [ $PATCH_SYSTEME -eq 0 ]
	then
		if [ "$PWD" != "$REP_DEST" ]
		then
			exec_CMD $LINENO cd $REP_DEST
		fi
		msg_echo $LINENO "patch -p1 < $REP_PATCH/$FIC"
		# echo DESACTIVE "patch -p1 < $REP_PATCH/$FIC"
		patch -p1 < $REP_PATCH/$FIC
		test_cr $LINENO $?
		msg_echo $LINENO "patch applique avec succes"
	else
		exec_CMD $LINENO cd $REP_PATCH
		# Decompression du patch
		exec_CMD $LINENO unzip -u $FIC
		# Recuperation du chemin a copier
		PATCH_NAME=$(basename $FIC .zip)
		if [ ! -d $REP_PATCH/$PATCH_NAME ]
		then
			# ERREUR
			msg_echo $LINENO "ERREUR Apres unzip de $REP_PATCH/$FIC on devrait trouver un repertoire $REP_PATCH/$PATCH_NAME"
			test_cr $LINENO 1
		fi
		PATCH_NAME_2=$(ls $REP_PATCH/$PATCH_NAME | grep patch)
		if [ "$PATCH_NAME_2" = "" ]
		then
			# ERREUR
			msg_echo $LINENO "ERREUR Impossible de trouver le repertoire commencant par \"patch\" dans le repertoire $REP_PATCH/$PATCH_NAME"
			test_cr $LINENO 1
		fi
		# Deploiement du patch
		exec_CMD $LINENO cp -r $REP_PATCH/$PATCH_NAME/$PATCH_NAME_2 $WSO2_HOME/repository/components/patches
	fi
	
	# Tagguer le fichier a la date et heure du jour
	msg_echo $LINENO "patch applique avec succes"
	exec_CMD $LINENO mv $REP_PATCH/$FIC $REP_PATCH/$FIC.$TAG_HORAIRE
	if [ -f $FIC_TEMPLATE -a "$FIC_TEMPLATE" != "" ]
	then
		exec_CMD $LINENO mv $FIC_TEMPLATE $FIC_TEMPLATE.$TAG_HORAIRE
	fi
done