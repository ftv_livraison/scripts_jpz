export FPATH=/appli/product/fonctions
init_envir $LINENO
EXIT_NOW=1

msg_echo $LINENO "Recuperation de la variable FTV_STORE_IPA_UUID"
FTV_STORE_IPA=$(basename $(ls -rt $WSO2_HOME/repository/deployment/server/jaggeryapps/publisher/upload/*FTVStore*.ipa | tail -1))
[ "$FTV_STORE_IPA" = "" ] && FTV_STORE_IPA=NON_TROUVE

eval $(echo "select 'FTV_STORE_IPA_UUID=' || reg_resource.reg_uuid as uuid
from reg_content
inner join reg_resource on reg_resource.reg_content_id = reg_content.reg_content_id
where utl_raw.cast_to_varchar2(dbms_lob.substr(reg_content_data, 500)) like '%publisher/upload/$FTV_STORE_IPA%';" \
| sqlplus $FTV_ORACLE_USR/$FTV_ORACLE_PWD@$FTV_ORACLE_DBNAME | grep FTV_STORE_IPA_UUID)
[ "$FTV_STORE_IPA_UUID" = "" ] && FTV_STORE_IPA_UUID=NON_TROUVE
msg_echo $LINENO "FTV_STORE_IPA_UUID=$FTV_STORE_IPA_UUID"
