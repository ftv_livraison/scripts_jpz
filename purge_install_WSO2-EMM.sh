liste_fic() {
# fichiers log des etapes
[ $(ls /appli/product/*.log 2> /dev/null | wc -l) -ne 0 ] && ls /appli/product/*.log
# Archive generee pour le reverse proxy
echo /appli/product/ReverseProxy.tar.gz
echo /appli/product/patchs/emm_truststore.bks
ls /appli/product/patchs/patch_EMM_*FTV* | grep -v "\.src"
# Fichiers du repertoire reverproxy
ls /appli/product/ReverseProxy/* \
| grep -vE ".png|.crt|.src|welcome.conf|.pdf"
}

liste_dir() {
# install Java
echo /appli/JDK/jdk1.7.0_67/
# install WSO2
echo /appli/WSO2-EMM/wso2emm-1.1.0/
# Repertoires des log
echo /appli/log/emm/
# Repertoire ou sont generes les certificats
echo /appli/WSO2-EMM/gen_certificates/
# repertoires ou ont ete decompresses les patchs
ls -l /appli/product/patchs/ \
| grep "^d" \
| grep -v agents_iOS_Android \
| awk '{ printf "/appli/product/patchs/%s\n", $9 }'
}

echo


echo "Recherche de process Java ..."
PID2KILL=$(ps -ef | grep -i java | grep -v grep | awk '{ print $2 }')
if [ "$PID2KILL" != "" ]
then
	if [ -f /appli/WSO2-EMM/wso2emm-1.1.0/bin/wso2server.sh ]
	then
		printf "	%s" "/appli/WSO2-EMM/wso2emm-1.1.0/bin/wso2server.sh stop "
		/appli/WSO2-EMM/wso2emm-1.1.0/bin/wso2server.sh stop
		[ $? -eq 0 ] && echo OK || echo KO
		sleep 5
		
		PID2KILL=$(ps -ef | grep -i java | grep -v grep | awk '{ print $2 }')
	fi
	
	if [ "$PID2KILL" != "" ]
	then
		printf "	%s" "kill -9 $PID2KILL "
		kill -9 $PID2KILL
		[ $? -eq 0 ] && echo OK || echo KO
	fi
else
	echo "	Pas de process Java a arreter"
fi
echo

echo "Sauvegarde des apk du store ..."
FTV_DIR_BACKUP_UPLOAD=/appli/product/backup_upload_$(uname -n)
WSO2_DIR_UPLOAD=/appli/WSO2-EMM/wso2emm-1.1.0/repository/deployment/server/jaggeryapps/publisher/upload
[ ! -d $FTV_DIR_BACKUP_UPLOAD ] && mkdir $FTV_DIR_BACKUP_UPLOAD || echo "	$FTV_DIR_BACKUP_UPLOAD existe deja"
if [ -d $WSO2_DIR_UPLOAD -a -d $WSO2_DIR_UPLOAD ]
then
	rm $FTV_DIR_BACKUP_UPLOAD/*
	cp -p $WSO2_DIR_UPLOAD/* $FTV_DIR_BACKUP_UPLOAD
else
	echo "	Rien a sauvegarder"
fi
echo

echo "Purge des Fichier ..."
liste_fic \
| grep -v "^$" \
| while read FIC
do
	if [ -f $FIC ]
	then
		printf "	%s" "rm $FIC "
		rm $FIC
		[ $? -eq 0 ] && echo OK || echo KO
	else
		echo "	$FIC non trouve"
	fi
done
echo

echo "Purge des repertoires ..."
liste_dir \
| while read REP
do
	if [ -d $REP ]
	then
		printf "	%s" "rm -Rf $REP "
		rm -Rf $REP
		[ $? -eq 0 ] && echo OK || echo KO
	else
		echo "	$REP non trouve"
	fi
done
echo
