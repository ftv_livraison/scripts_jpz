#!/bin/ksh
# Generation du script de deploiements des fichiers necessaires au reverse proxy
# dans le repertoire /appli/product/ReverseProxy ($FTV_DIR_REVERSEPROXY)
# Le repertoire $FTV_DIR_REVERSEPROXY sera ensuite archive dans un fichier tar 
# puis sera compresse par gzip (tar.gz)

export FPATH=/appli/product/fonctions 

init_envir $LINENO
EXIT_NOW=1

template_2_file $LINENO $FTV_DIR_REVERSEPROXY/wso2_emm.conf.src $FTV_DIR_REVERSEPROXY/wso2_emm.conf
template_2_file $LINENO $FTV_DIR_REVERSEPROXY/httpd_EMRP.src    $FTV_DIR_REVERSEPROXY/httpd_EMRP


cat << %FIN% > $FTV_SH_REVERSEPROXY
#!/bin/ksh
# Ce script a ete genere par $SELF sur $SELF_HOST
# Le $(date "+ %d/%m/%Y a %H:%M:%S")
# Fonction de test du code retour
test_cr() {
	[ \$1 -eq 0 ] && echo \"OK\" || echo \"ERREUR\"
}
%FIN%
test_cr $LINENO $?

echo "\
$FTV_DIR_REVERSEPROXY/httpd_EMRP                       /etc/logrotate.d/
$FTV_DIR_REVERSEPROXY/$FTV_LOGO_FILENAME               /var/www/html/
$FTV_DIR_REVERSEPROXY/qr_code_$FTV_ENVIRONNEMENT.png   /var/www/html/
$FTV_DIR_REVERSEPROXY/FTVchaines_fondblanc.png         /var/www/html/
$FTV_DIR_REVERSEPROXY/ROOTFTV.crt                      /var/www/html/
$FTV_DIR_REVERSEPROXY/$FTV_MODOP_ANDROID_FILENAME      /var/www/html/
$FTV_DIR_REVERSEPROXY/$FTV_MODOP_APPLE_FILENAME        /var/www/html/
$FTV_DIR_REVERSEPROXY/wso2_emm.conf                    /etc/httpd/conf.d/
$FTV_DIR_REVERSEPROXY/welcome.conf                     /etc/httpd/conf.d/
$WSO2_GEN_CERT_DIR/ca_cert.pem                         /etc/httpd/conf.d/
$WSO2_GEN_CERT_DIR/ia.crt                              /etc/httpd/conf.d/
$WSO2_GEN_CERT_DIR/ia_nopwd.key                        /etc/httpd/conf.d/
" \
| while read FIC DEST
do
	[ "$FIC" = "" ] && break
	if [ ! -f $FTV_DIR_REVERSEPROXY/$(basename $FIC) -a -f $FIC ]
	then
		exec_CMD $LINENO cp $FIC $FTV_DIR_REVERSEPROXY/
	fi
cat << %FIN% >> $FTV_SH_REVERSEPROXY
# Si le fichier $(basename $FIC) existe dans $FTV_DIR_REVERSEPROXY
[ -f $FTV_DIR_REVERSEPROXY/$(basename $FIC) ] && {
	# On le copie dans $DEST
	echo cp $FTV_DIR_REVERSEPROXY/$(basename $FIC) $DEST
	cp $FTV_DIR_REVERSEPROXY/$(basename $FIC) $DEST
	test_cr \$?
} || {
	# Sinon on informe
	echo $FTV_DIR_REVERSEPROXY/$(basename $FIC) non trouve
}
%FIN%
done
cat << %FIN% >> $FTV_SH_REVERSEPROXY
# Recuperation de l etat de httpd_can_network_connect
httpd_can_network_connect=\$(getsebool httpd_can_network_connect | cut -d' ' -f3)
# Si httpd_can_network_connect n est pas "on"
[ \"\$httpd_can_network_connect\" != \"on\" ] && {
	echo setsebool -P httpd_can_network_connect on
	setsebool -P httpd_can_network_connect on
	test_cr \$?
} || {
	# On ne le refait pas 2 fois (trop long)
	echo httpd_can_network_connect deja a on
}
echo apachectl restart
apachectl restart
test_cr \$?
%FIN%

exec_CMD $LINENO chmod uog+x $FTV_SH_REVERSEPROXY

exec_CMD $LINENO cd $FTV_DIR_PRODUCT
exec_CMD $LINENO tar cvf ReverseProxy.tar ReverseProxy/
exec_CMD $LINENO gzip -vf9 ReverseProxy.tar