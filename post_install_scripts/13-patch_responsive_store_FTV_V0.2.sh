export FPATH=/appli/product/fonctions
init_envir $LINENO
EXIT_NOW=1

template_2_file $LINENO $FTV_DIR_PATCH/patch_responsive_store_FTV_V0.2.src $FTV_DIR_PATCH/patch_responsive_store_FTV_V0.2

exec_CMD $LINENO type patch > /dev/null

exec_CMD $LINENO cd $WSO2_HOME
msg_echo $LINENO "patch -p1 < $FTV_DIR_PATCH/patch_responsive_store_FTV_V0.2"
patch -p1 < $FTV_DIR_PATCH/patch_responsive_store_FTV_V0.2
test_cr $LINENO $?