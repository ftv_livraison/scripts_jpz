export FPATH=/appli/product/fonctions
init_envir $LINENO
EXIT_NOW=1

exec_CMD $LINENO cd $FTV_DIR_PATCH
exec_CMD $LINENO unzip -u $FTV_DIR_PATCH/WSO2-CARBON-PATCH-4.2.0-0208.zip

if [ ! -d $WSO2_HOME/repository/components/patches/patch0208/ ]
then
	exec_CMD $LINENO mkdir $WSO2_HOME/repository/components/patches/patch0208/
fi
exec_CMD $LINENO cp $FTV_DIR_PATCH/WSO2-CARBON-PATCH-4.2.0-0208/patch0208/org.wso2.carbon.identity.oauth_4.2.2.jar $WSO2_HOME/repository/components/patches/patch0208/