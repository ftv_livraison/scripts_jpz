export FPATH=/appli/product/fonctions
init_envir $LINENO
EXIT_NOW=1

if [ ! -d $FTV_DIR_PATCH/WSO2_MDM_Connector_for_iOS ]
then
	exec_CMD $LINENO mkdir $FTV_DIR_PATCH/WSO2_MDM_Connector_for_iOS
fi
exec_CMD $LINENO cd $FTV_DIR_PATCH/WSO2_MDM_Connector_for_iOS
exec_CMD $LINENO unzip -u $FTV_DIR_PATCH/WSO2_MDM_Connector_for_iOS.zip

echo "\
$FTV_DIR_PATCH/WSO2_MDM_Connector_for_iOS/org.wso2.carbon.emm.ios.apns-1.1.0.jar    $WSO2_HOME/repository/components/dropins/org.wso2.carbon.emm.ios.apns-1.1.0.jar
$FTV_DIR_PATCH/WSO2_MDM_Connector_for_iOS/org.wso2.carbon.emm.ios.core-1.1.0.jar    $WSO2_HOME/repository/components/dropins/org.wso2.carbon.emm.ios.core-1.1.0.jar
$FTV_DIR_PATCH/WSO2_MDM_Connector_for_iOS/org.wso2.carbon.emm.ios.payload-1.1.0.jar $WSO2_HOME/repository/components/dropins/org.wso2.carbon.emm.ios.payload-1.1.0.jar
" \
| grep -v "^$" \
| while read FIC_2_COPY DESTINATION
do
	exec_CMD $LINENO cp $FIC_2_COPY $DESTINATION
done