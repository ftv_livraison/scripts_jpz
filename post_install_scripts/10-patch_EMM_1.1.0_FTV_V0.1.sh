export FPATH=/appli/product/fonctions
init_envir $LINENO
EXIT_NOW=1

template_2_file $LINENO $FTV_DIR_PATCH/patch_EMM_1.1.0_FTV_V0.1.src $FTV_DIR_PATCH/patch_EMM_1.1.0_FTV_V0.1

exec_CMD $LINENO type patch > /dev/null

exec_CMD $LINENO cd $WSO2_HOME
msg_echo $LINENO "patch -p1 < $FTV_DIR_PATCH/patch_EMM_1.1.0_FTV_V0.1"
patch -p1 < $FTV_DIR_PATCH/patch_EMM_1.1.0_FTV_V0.1
test_cr $LINENO $?