export FPATH=/appli/product/fonctions
init_envir $LINENO
EXIT_NOW=1

exec_CMD $LINENO cd $FTV_DIR_PATCH
exec_CMD $LINENO unzip -u $FTV_DIR_PATCH/WSO2-CARBON-PATCH-4.2.0-1090.zip

echo "\
$FTV_DIR_PATCH/WSO2-CARBON-PATCH-4.2.0-1090/resources/apimgt_oracle.sql $WSO2_HOME/dbscripts/apimgt/oracle.sql
$FTV_DIR_PATCH/WSO2-CARBON-PATCH-4.2.0-1090/resources/mysql.sql         $WSO2_HOME/dbscripts/emm/mysql.sql
$FTV_DIR_PATCH/WSO2-CARBON-PATCH-4.2.0-1090/resources/emm_oracle.sql    $WSO2_HOME/dbscripts/emm/oracle.sql
$FTV_DIR_PATCH/WSO2-CARBON-PATCH-4.2.0-1090/resources/resource.sql      $WSO2_HOME/dbscripts/storage/oracle/resource.sql
$FTV_DIR_PATCH/WSO2-CARBON-PATCH-4.2.0-1090/resources/user.js           $WSO2_HOME/repository/deployment/server/jaggeryapps/emm/modules/user.js
$FTV_DIR_PATCH/WSO2-CARBON-PATCH-4.2.0-1090/resources/device.js         $WSO2_HOME/repository/deployment/server/jaggeryapps/emm/modules/device.js
$FTV_DIR_PATCH/WSO2-CARBON-PATCH-4.2.0-1090/resources/oracle_db.js      $WSO2_HOME/repository/deployment/server/jaggeryapps/emm/sqlscripts/db.js
$FTV_DIR_PATCH/WSO2-CARBON-PATCH-4.2.0-1090/resources/h2_mysql_db.js    $WSO2_HOME/repository/deployment/server/jaggeryapps/emm/sqlscripts/h2_mysql_db.js
$FTV_DIR_PATCH/WSO2-CARBON-PATCH-4.2.0-1090/resources/mdm_reports.js    $WSO2_HOME/repository/deployment/server/jaggeryapps/emm/modules/mdm_reports.js
$FTV_DIR_PATCH/WSO2-CARBON-PATCH-4.2.0-1090/resources/store.js          $WSO2_HOME/repository/deployment/server/jaggeryapps/emm/modules/store.js
$FTV_DIR_PATCH/WSO2-CARBON-PATCH-4.2.0-1090/resources/query.provider.js $WSO2_HOME/repository/deployment/server/jaggeryapps/publisher/modules/data/extensions/oracle/query.provider.js
$FTV_DIR_PATCH/WSO2-CARBON-PATCH-4.2.0-1090/resources/model.manager.js  $WSO2_HOME/repository/deployment/server/jaggeryapps/publisher/modules/data/model.manager.js
" \
| grep -v "^$" \
| while read FIC_2_COPY DESTINATION
do
	exec_CMD $LINENO cp $FIC_2_COPY $DESTINATION
done