export FPATH=/appli/product/fonctions
init_envir $LINENO
EXIT_NOW=1

msg_echo $LINENO "Arret de WSO2"
exec_CMD $LINENO $WSO2_HOME/bin/wso2server.sh stop

exec_CMD $LINENO cp $FTV_DIR_PATCH/France_televisions_logo.png $WSO2_HOME/repository/deployment/server/jaggeryapps/store/themes/store/img/

msg_echo $LINENO "Recuperation de la variable FTV_STORE_IPA_UUID"
FTV_STORE_IPA=$(basename $(ls -rt $WSO2_HOME/repository/deployment/server/jaggeryapps/publisher/upload/*FTVStore*.ipa | tail -1))
[ "$FTV_STORE_IPA" = "" ] && FTV_STORE_IPA=NON_TROUVE

eval $(echo "select 'FTV_STORE_IPA_UUID=' || reg_resource.reg_uuid as uuid
from reg_content
inner join reg_resource on reg_resource.reg_content_id = reg_content.reg_content_id
where utl_raw.cast_to_varchar2(dbms_lob.substr(reg_content_data, 500)) like '%publisher/upload/$FTV_STORE_IPA%';" \
| sqlplus $FTV_ORACLE_USR/$FTV_ORACLE_PWD@$FTV_ORACLE_DBNAME | grep FTV_STORE_IPA_UUID)
[ "$FTV_STORE_IPA_UUID" = "" ] && FTV_STORE_IPA_UUID=NON_TROUVE
msg_echo $LINENO "FTV_STORE_IPA_UUID=$FTV_STORE_IPA_UUID"

msg_echo $LINENO "Recuperation de la variable FTV_STORE_APK_FILENAME"
FTV_STORE_APK_FILENAME=$(basename $(ls -rt $WSO2_HOME/repository/deployment/server/jaggeryapps/publisher/upload/*FTVStore*apk | tail -1))
[ "$FTV_STORE_APK_FILENAME" = "" ] && FTV_STORE_APK_FILENAME=NON_TROUVE
msg_echo $LINENO "FTV_STORE_APK_FILENAME=$FTV_STORE_APK_FILENAME"

template_2_file $LINENO $FTV_DIR_PATCH/patch_responsive_store_FTV_V0.3.src $FTV_DIR_PATCH/patch_responsive_store_FTV_V0.3

exec_CMD $LINENO type patch > /dev/null

exec_CMD $LINENO cd $WSO2_HOME
msg_echo $LINENO "patch -p1 < $FTV_DIR_PATCH/patch_responsive_store_FTV_V0.3"
patch -p1 < $FTV_DIR_PATCH/patch_responsive_store_FTV_V0.3
test_cr $LINENO $?

msg_echo $LINENO "Verification de la presence l'alias ROOTCAFTV ..."
keytool -list -alias ROOTCAFTV -keystore $WSO2_HOME/repository/resources/security/client-truststore.jks -file $FTV_DIR_REVERSEPROXY/ROOTFTV.crt -storepass $FTV_CARBON_PASSWORD
if [ $? -ne 0 ]
then
msg_echo $LINENO "keytool -import -alias ROOTCAFTV -keystore $WSO2_HOME/repository/resources/security/client-truststore.jks -file $FTV_DIR_REVERSEPROXY/ROOTFTV.crt -storepass $FTV_CARBON_PASSWORD"
echo yes | keytool -import -alias ROOTCAFTV -keystore $WSO2_HOME/repository/resources/security/client-truststore.jks -file $FTV_DIR_REVERSEPROXY/ROOTFTV.crt -storepass $FTV_CARBON_PASSWORD
test_cr $LINENO $?
else
	msg_echo $LINENO "Alias ROOTCAFTV deja present."
fi

msg_echo $LINENO "Demarrage de WSO2"
exec_CMD $LINENO $WSO2_HOME/bin/wso2server.sh start