export FPATH=/appli/product/fonctions
init_envir $LINENO
EXIT_NOW=1

exec_CMD $LINENO cd $FTV_DIR_PATCH
exec_CMD $LINENO unzip -u $FTV_DIR_PATCH/WSO2-CARBON-PATCH-4.2.0-1031.zip

if [ ! -d $WSO2_HOME/repository/components/patches/patch1031/ ]
then
	exec_CMD $LINENO mkdir $WSO2_HOME/repository/components/patches/patch1031/
fi 
exec_CMD $LINENO cp $FTV_DIR_PATCH/WSO2-CARBON-PATCH-4.2.0-1031/patch1031/org.wso2.carbon.user.core_4.2.0.jar $WSO2_HOME/repository/components/patches/patch1031/