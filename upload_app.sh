# Request
curl -k -X POST "https://localhost:9443/publisher/api/authenticate" -F "username=admin" -F "password=admin" -F "action=login"

# Respone:
# {"data" : {"sessionId" : "B50BD5056BF2AEF94FA63917A4C180C8"}}

# To Upload An mobile application application
# Request - 
curl -k -X POST -H "Cache-Control: no-cache" -H "Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW" -F "file=@android-sample-android.apk" 'https://localhost:9443/publisher/api/mobileapp/upload' -b 'JSESSIONID=B50BD5056BF2AEF94FA63917A4C180C8'

# Reply - 
# {"path" : "/publisher/upload/VK0Otandroid-sample-android.apk", "package" : "com.gmail.yuyang226.flickrj.sample.android", "version" : "1.0"}

# The above reply would be the input as appmeta to add an asset.

To Add an asset on publisher
Request - 
curl -k -X POST -H "Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW" -F "description=description" -F "version=1.0" -F "provider=1WSO2Mobile" -F "name=APPNAME" -F "screenshot1File=@screenshot.jpg" -F "markettype=Enterprise" -F "appmeta={\"path\" : \"/publisher/upload/VK0Otandroid-sample-android.apk\", \"package\" : \"com.gmail.yuyang226.flickrj.sample.android\", \"version\" : \"1.0\"}" -F "bannerFile=@banner.jpg" -F "iconFile=@icon.jpg" -F "type=mobileapp" -F "platform=android" 'http://localhost:9763/publisher/api/asset/mobileapp' -b 'JSESSIONID=B50BD5056BF2AEF94FA63917A4C180C8'

# Response -
# {"ok" : "true", "message" : "Asset created.", "id" : "fd34b86a-75b4-44d3-bb96-fd276fe97b22"}