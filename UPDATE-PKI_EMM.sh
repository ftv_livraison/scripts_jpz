#!/bin/ksh
# Deploiement des agents
# Les agents configures dans $WSO2_AGENTS sont deployes dans WSO2-EMM
export FPATH=/appli/product/fonctions
 
init_envir $LINENO
EXIT_NOW=1
 
if [ ! -d $WSO2_GEN_CERT_DIR ]
then
       exec_CMD $LINENO mkdir $WSO2_GEN_CERT_DIR
fi
 
exec_CMD $LINENO cd $WSO2_GEN_CERT_DIR

msg_echo $LINENO "openssl req -new -key ca_private.key -out ca.csr -subj \"/C=FR/ST=IDF/L=Paris/O=France Televisions/OU=IT/CN=$FTV_EMMCA_CERTALIAS/emailAddress=DeviceEnrollment@francetv.fr\""
openssl req -new -key ca_private.key -out ca.csr -subj "/C=FR/ST=IDF/L=Paris/O=France Televisions/OU=IT/CN=$FTV_EMMCA_CERTALIAS/emailAddress=DeviceEnrollment@francetv.fr"
test_cr $LINENO $?
exec_CMD $LINENO openssl x509 -req -days 3650 -in ca.csr -signkey ca_private.key -out ca.crt -extensions v3_ca -extfile $FTV_DIR_PATCH/openssl.cnf
exec_CMD $LINENO openssl rsa -in ca_private.key -text > ca_private.pem
exec_CMD $LINENO openssl x509 -in ca.crt -out ca_cert.pem
 
# Generation de la RA
[ ! -f ra_private.key ] && exec_CMD $LINENO openssl genrsa -out ra_private.key 4096
msg_echo $LINENO "openssl req -new -key ra_private.key -out ra.csr -subj \"/C=FR/ST=IDF/L=Paris/O=France Televisions/OU=IT/CN=$FTV_EMMRA_CERTALIAS/emailAddress=DeviceEnrollment@francetv.fr\""
openssl req -new -key ra_private.key -out ra.csr -subj "/C=FR/ST=IDF/L=Paris/O=France Televisions/OU=IT/CN=$FTV_EMMRA_CERTALIAS/emailAddress=DeviceEnrollment@francetv.fr"
test_cr $LINENO $?
exec_CMD $LINENO openssl x509 -req -days 3650 -in ra.csr -CA ca.crt -CAkey ca_private.key -set_serial 02 -out ra.crt -extensions v3_req -extfile $FTV_DIR_PATCH/openssl.cnf
exec_CMD $LINENO openssl rsa -in ra_private.key -text > ra_private.pem
exec_CMD $LINENO openssl x509 -in ra.crt -out ra_cert.pem
 
# Generation du certificat SSL de communication HTTPS
[ ! -f ia.key ] && exec_CMD $LINENO openssl genrsa -out ia.key 4096
msg_echo $LINENO "openssl req -new -key ia.key -out ia.csr -subj \"/C=FR/ST=IDF/L=Paris/O=France Televisions/OU=IT/CN=$FTV_STORE_URL/emailAddress=DeviceEnrollment@francetv.fr\""
openssl req -new -key ia.key -out ia.csr -subj "/C=FR/ST=IDF/L=Paris/O=France Televisions/OU=IT/CN=$FTV_STORE_URL/emailAddress=DeviceEnrollment@francetv.fr"
test_cr $LINENO $?
exec_CMD $LINENO openssl x509 -req -days 3650  -in ia.csr -CA ca_cert.pem -CAkey ca_private.pem -set_serial 044324343 -out ia.crt
 
# Recuperation / deploiement des certificats et clefs privees
exec_CMD $LINENO openssl rsa -in ia.key -out ia_nopwd.key
 
# Export des cles dans les p12 distincts pour sauvegarde
exec_CMD $LINENO openssl pkcs12 -export -out KEYSTORE.p12 -inkey ia.key -in ia.crt -CAfile ca_cert.pem -name $FTV_STORE_URL -passin pass:$FTV_CARBON_PASSWORD -passout pass:$FTV_CARBON_PASSWORD
exec_CMD $LINENO openssl pkcs12 -export -out ca.p12 -inkey ca_private.pem -in ca_cert.pem -CAfile ssl.crt -name cacert -passin pass:$FTV_EMM_PASSWORD -passout pass:$FTV_EMM_PASSWORD
exec_CMD $LINENO openssl pkcs12 -export -out ra.p12 -inkey ra_private.pem -in ra_cert.pem -CAfile ca_cert.pem -name racert -passin pass:$FTV_EMM_PASSWORD -passout pass:$FTV_EMM_PASSWORD
 
# Import de CA.p12/RA.p12 dans wso2emm.jks
exec_CMD $LINENO rm $WSO2_HOME/repository/resources/security/wso2emm.jks
exec_CMD $LINENO keytool -importkeystore -srckeystore ra.p12 -srcstoretype pkcs12 -destkeystore $WSO2_HOME/repository/resources/security/wso2emm.jks -destalias $FTV_EMMRA_CERTALIAS -srcalias racert -srcstorepass $FTV_EMM_PASSWORD -deststorepass $FTV_EMM_PASSWORD
exec_CMD $LINENO keytool -importkeystore -srckeystore ca.p12 -srcstoretype pkcs12 -destkeystore $WSO2_HOME/repository/resources/security/wso2emm.jks -destalias $FTV_EMMCA_CERTALIAS -srcalias cacert -srcstorepass $FTV_EMM_PASSWORD -deststorepass $FTV_EMM_PASSWORD
 
# Import de KEYSTORE.p12/CA.p12/RA.p12 dans wso2carbon.jks
exec_CMD $LINENO keytool -importkeystore -srckeystore KEYSTORE.p12 -srcstoretype pkcs12 -destkeystore $WSO2_HOME/repository/resources/security/wso2carbon.jks -destalias $FTV_STORE_URL -srcalias $FTV_STORE_URL -srcstorepass $FTV_CARBON_PASSWORD -deststorepass $FTV_CARBON_PASSWORD
 
# Import de KEYSTORE.p12/CA.p12/RA.p12 dans client-truststore.jks :
exec_CMD $LINENO keytool -importkeystore -srckeystore KEYSTORE.p12 -srcstoretype pkcs12 -destkeystore $WSO2_HOME/repository/resources/security/client-truststore.jks -destalias $FTV_STORE_URL -srcalias $FTV_STORE_URL -srcstorepass $FTV_CARBON_PASSWORD -deststorepass $FTV_CARBON_PASSWORD

exec_CMD $LINENO cp $FTV_DIR_PATCH/bcprov-ext-jdk15on-1.46.jar $JAVA_HOME/jre/lib/ext/
	
JAVA_RESSOURCE=org.bouncycastle.jce.provider.BouncyCastleProvider

msg_echo $LINENO "Recherche de security.provider.*[0-9]=$JAVA_RESSOURCE dans le fichier $JAVA_HOME/jre/lib/security/java.security"
CHECK_JAVA=$(grep -cE "security.provider.*[0-9]=$JAVA_RESSOURCE" $JAVA_HOME/jre/lib/security/java.security)
if [ $CHECK_JAVA -eq 0 ]
then
	msg_echo $LINENO "La ressource n'a pas ete trouvee"
	msg_echo $LINENO "Recherche du dernier numero de security.provider"
	LAST_NB_LINE=$(grep -E "security.provider.*[0-9]=" $JAVA_HOME/jre/lib/security/java.security | cut -d. -f3 | cut -d= -f1 | tail -1)
	[ "$LAST_NB_LINE" = "" ] && LAST_NB_LINE=0
	msg_echo $LINENO "Increment du dernier numero de security.provider ($LAST_NB_LINE + 1)"
	LAST_NB_LINE=$(( $LAST_NB_LINE + 1 ))
	test_cr $LINENO $?
	msg_echo $LINENO "Ajout de la ligne security.provider.$LAST_NB_LINE=$JAVA_RESSOURCE dans le fichier $JAVA_HOME/jre/lib/security/java.security"
	echo security.provider.$LAST_NB_LINE=$JAVA_RESSOURCE >> $JAVA_HOME/jre/lib/security/java.security
	test_cr $LINENO $?
else
	msg_echo $LINENO "Modification de la configuration Java deja realisee."
fi
msg_echo $LINENO "Recherche de bcprov-ext-jdk15on-1.46.jar dans la variable CLASSPATH"
if [ $(echo $CLASSPATH | grep -c bcprov-ext-jdk15on-1.46.jar) -eq 0 ]
then
	msg_echo $LINENO "Ajout de $FTV_DIR_PATCH/bcprov-ext-jdk15on-1.46.jar a CLASSPATH"
	[ "$CLASSPATH" = "" ] && export CLASSPATH=$FTV_DIR_PATCH/bcprov-ext-jdk15on-1.46.jar || export CLASSPATH=$CLASSPATH:$FTV_DIR_PATCH/bcprov-ext-jdk15on-1.46.jar
	test_cr $LINENO $?
else
	msg_echo $LINENO "$FTV_DIR_PATCH/bcprov-ext-jdk15on-1.46.jar est deja dans CLASSPATH"
fi
msg_echo $LINENO "CLASSPATH=$CLASSPATH"
 
msg_echo $LINENO "Generation du certificat au format Android"
if [ -f $FTV_DIR_PATCH/emm_truststore.bks ]
then
	exec_CMD $LINENO cp -p $FTV_DIR_PATCH/emm_truststore.bks $FTV_DIR_PATCH/emm_truststore.bks.$(date +%Y%m%d)
fi
exec_CMD $LINENO rm -f $FTV_DIR_PATCH/emm_truststore.bks
exec_CMD $LINENO keytool -noprompt -importcert -v -trustcacerts -file $WSO2_GEN_CERT_DIR/ca_cert.pem -alias BKS -keystore $FTV_DIR_PATCH/emm_truststore.bks -provider org.bouncycastle.jce.provider.BouncyCastleProvider -providerpath "bcprov-jdk16-145.jar" -storetype BKS -storepass $FTV_CARBON_PASSWORD
exec_CMD $LINENO cp -p $FTV_DIR_PATCH/emm_truststore.bks $FTV_DIR_BACKUP_CERT

exec_cmd $LINENO cp -p $WSO2_GEN_CERT_DIR/ia.crt       $FTV_DIR_REVERSEPROXY
exec_cmd $LINENO cp -p $WSO2_GEN_CERT_DIR/ia_nopwd.key $FTV_DIR_REVERSEPROXY
exec_cmd $LINENO cp -p $WSO2_GEN_CERT_DIR/ca_cert.pem  $FTV_DIR_REVERSEPROXY