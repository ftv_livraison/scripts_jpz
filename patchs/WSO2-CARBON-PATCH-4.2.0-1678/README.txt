Patch ID         : WSO2-CARBON-PATCH-4.2.0-1678
Applies To       : EMM 1.1.0
Associated JIRAS : 

https://wso2.org/jira/browse/EMM-1070

DESCRIPTION
-------------

When it is enforced a passcode policy via a device policy to an iOS device, user is allowed to remove it manually from the device settings without harming to the applied policy. Hence the server does not detect that change and still shows as a compliant device.
In the ideal situation that option to remove passcode should be disabled in the device.

INSTALLATION INSTRUCTIONS
---------------------------

(01) Shutdown the server, if you have already started.

(02) Copy the following files provided in resources folder of the patch to specified locations inside the server.

	 (i)	org.wso2.carbon.emm.ios.payload-1.1.0.jar ---> <emm_server>/repository/components/dropins/
	 (ii)	org.wso2.carbon.emm.ios.core-1.1.0.jar ---> <emm_server>/repository/components/dropins/

(03) Restart the server with one of the following commands.
     Linux/Unix :  sh wso2server.sh
     Windows    :  wso2server.bat

