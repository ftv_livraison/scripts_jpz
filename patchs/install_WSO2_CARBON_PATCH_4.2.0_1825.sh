#!/bin/ksh
export FPATH=/appli/product/fonctions

# Initialisation de l'environnement
init_envir $LINENO
EXIT_NOW=1
EXIT=0

# Recuperation des parametres
PATCH_PATH=$1
PATCH_FILE=$2
# Controle des parametres
for VAR in PATCH_PATH PATCH_FILE 
do
	check_var $LINENO $VAR
	[ $? -ne 0 ] && EXIT=1
done

PATCH_NAME=`basename ${PATCH_FILE} .zip`
# Controle des autres variables
for VAR in PATCH_NAME WSO2_HOME
do
	check_var $LINENO $VAR
	[ $? -ne 0 ] && EXIT=1
done
# s'il manque une variable, on sors
[ $EXIT -ne 0 ] && test_cr $LINENO $EXIT $EXIT_NOW

# Repertoire du patch
exec_CMD $LINENO cd $PATCH_PATH

# Est-ce que le patch existe ?
if [ ! -f "$PATCH_FILE" ];
then
	msg_echo $LINENO "Patch file \"${PATCH_FILE}\" does not exist in \"$PATCH_PATH\"."
	test_cr $LINENO 1 $EXIT_NOW
fi
# Decompression du patch
exec_CMD $LINENO unzip $PATCH_FILE
# Deploiement du patch
exec_CMD $LINENO cp -r $PATCH_PATH/$PATCH_NAME/patch1825 $WSO2_HOME/repository/components/patches/