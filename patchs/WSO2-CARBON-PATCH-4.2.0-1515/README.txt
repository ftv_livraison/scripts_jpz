Patch ID         : WSO2-CARBON-PATCH-4.2.0-1515
Applies To       : EMM 1.1.0
Associated JIRAS : https://wso2.org/jira/browse/EMM-807


DESCRIPTION
-------------
Some client may require to stop displaying store apps in the homepage slider for anonymous users. This patch provides a configurable feature to display/block 
app visibility for anonymous users.


INSTALLATION INSTRUCTIONS
---------------------------

(01) Shutdown the server, if you have already started.

(02) Copy the following files provided in resources folder of the patch to specified locations inside the server.

     (i) 	modules/store.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/modules/

     (ii) 	apis/assets.jag ---> <emm_server>/repository/deployment/server/jaggeryapps/store/apis/

     (iii) 	modules/store.js ---> <emm_server>/repository/deployment/server/jaggeryapps/store/modules/

     (iv) 	config/store.json ---> <emm_server>/repository/deployment/server/jaggeryapps/store/config/

(03) Restart the server with :
	Linux/Unix : sh wso2server.sh
	Windows : wso2server.bat

