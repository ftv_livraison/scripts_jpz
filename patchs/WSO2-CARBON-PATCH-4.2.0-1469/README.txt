Patch ID         : WSO2-CARBON-PATCH-4.2.0-1469
Applies To       : EMM 1.1.0
Associated JIRAS  : 
https://wso2.org/jira/browse/EMM-803


DESCRIPTION
-------------

This patch addresses device enroll issues related to iOS and Android app installation issues due to oracle database support.


INSTALLATION INSTRUCTIONS
---------------------------

(01) Copy the following files provided in resources folder of the patch to specified locations inside the server.
     Prior to doing so you may keep a back up of the originals if neccessery.

     (i) 	devicelogin.jag ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/ios_utils/devicelogin.jag

     (ii) 	devicestatusmonitor.jag ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/ios_utils/devicestatusmonitor.jag

     (iii) 	enroll.jag ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/ios_utils/enroll.jag

     (iv) 	device.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/modules/device.js

     (v)        user.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/modules/user.js

     (vi)	db.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/sqlscripts/db.js

     (vii)	h2_mysql_db.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/sqlscripts/h2_mysql_db.js

     (viii)	oracle_db.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/sqlscripts/oracle_db.js

(02) Replace the content of db.js with either h2_mysql_db.js or oracle_db.js according to the EMM database support necessery

(04) Restart the server with one of the following commands.
     Linux/Unix :  sh wso2server.sh
     Windows    :  wso2server.bat


