Patch ID         : WSO2-CARBON-PATCH-4.2.0-1434
Applies To       : EMM 1.1.0
Associated JIRAS  : 
https://wso2.org/jira/browse/EMM-801,
https://wso2.org/jira/browse/EMM-686, 
https://wso2.org/jira/browse/EMM-802


DESCRIPTION
-------------

This patch addresses APNS update failing due to oracle query issues, agent upgrading issues and app policy not getting applied to device issues due to oracle database support.


INSTALLATION INSTRUCTIONS
---------------------------

(01) Copy the following files provided in resources folder of the patch to specified locations inside the server.
     Prior to doing so you may keep a back up of the originals if neccessery.

     (i) 	oracle_db.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/sqlscripts/

     (ii) 	add-mobileapp.hbs ---> <emm_server>/repository/deployment/server/jaggeryapps/publisher/themes/mobileapp/partials

     (iii) 	create.mobileapp.js ---> <emm_server>/repository/deployment/server/jaggeryapps/publisher/themes/mobileapp/js/mobileapp/

     (iv) 	common.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/client/js/

     (v)        device.js, user.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/modules/

     (vi)	enroll.jag ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/ios_utils/

     (vii)	install.js ---> <emm_server>/repository/deployment/server/jaggeryapps/publisher/config/defaults/

     (viii)	publisher.js ---> <emm_server>/repository/deployment/server/jaggeryapps/publisher/modules/

(02) Delete "db.js" file that already resides in <emm_server>/repository/deployment/server/jaggeryapps/emm/sqlscripts/.

(03) Rename <emm_server>/repository/deployment/server/jaggeryapps/emm/sqlscripts/oracle_db.js to "db.js".

(04) Restart the server with one of the following commands.
     Linux/Unix :  sh wso2server.sh
     Windows    :  wso2server.bat


