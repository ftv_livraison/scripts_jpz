Patch ID         : WSO2-CARBON-PATCH-4.2.0-1547
Applies To       : EMM 1.1.0
Associated JIRAS : https://wso2.org/jira/browse/EMM-809


DESCRIPTION
-------------
When an app update is pushed, some apps fail to upgrade due to version issues. This happens intermittently. There's another issue in this, where app get option of store to emm fails due to permission issues. This patch provides the solution to the above issue.


INSTALLATION INSTRUCTIONS
---------------------------

(01) Shutdown the server, if you have already started.

(02) Copy the wso2carbon-version.txt file to <CARBON_SERVER>/bin.

(03) Copy the following files provided in resources folder of the patch to specified locations inside the server.

     (i) 	resources/emm/modules/store.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/modules/
    
     (ii) 	resources/emm/sqlscripts/db.js, resources/emm/sqlscripts/h2_mysql_db.js, resources/emm/sqlscripts/oracle_db.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/sqlscripts/

     (iii) 	resources/store/apis/asset.jag ---> <emm_server>/repository/deployment/server/jaggeryapps/store/apis/

     (iv) 	resources/store/modules/store.js ---> <emm_server>/repository/deployment/server/jaggeryapps/store/modules/

(04) Restart the server with :
	Linux/Unix : sh wso2server.sh
	Windows : wso2server.bat

