Patch ID         : WSO2-CARBON-PATCH-4.2.0-0343
Applies To       : G-Reg 4.6.0
Associated JIRA  : https://wso2.org/jira/browse/REGISTRY-2183


DESCRIPTION
-----------
This patch fixes CarbonContext refactoring changes in org.wso2.carbon.registry.server-4.2.0, org.wso2.carbon.registry.resource-4.2.0 components.


INSTALLATION INSTRUCTIONS
-------------------------

(i)  Shutdown the server, if you have already started.

(ii) Copy the wso2carbon-version.txt file to <CARBON_SERVER>/bin.

(iii) Copy the patch0343 to  <CARBON_SERVER>/repository/components/patches/

(iv) Restart the server with :
       Linux/Unix :  sh wso2server.sh
       Windows    :  wso2server.bat

