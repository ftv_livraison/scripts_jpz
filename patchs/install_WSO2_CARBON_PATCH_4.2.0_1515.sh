#!/bin/sh

PATCH_PATH=$1
EMM_PATH=$2
PATCH_FILE=$3
PATCH_NAME=`basename ${PATCH_FILE} .zip`

cd ${PATCH_PATH}

if [ ! -f "${PATCH_FILE}" ];
then
	echo "Patch file "${PATCH_FILE}" does not exist."
	exit 1;
fi

unzip ${PATCH_FILE}

cp ${PATCH_PATH}/${PATCH_NAME}/resources/store/modules/store.js ${EMM_PATH}/repository/deployment/server/jaggeryapps/store/modules/
cp ${PATCH_PATH}/${PATCH_NAME}/resources/store/apis/assets.jag ${EMM_PATH}/repository/deployment/server/jaggeryapps/store/apis/
cp ${PATCH_PATH}/${PATCH_NAME}/resources/store/config/store.json ${EMM_PATH}/repository/deployment/server/jaggeryapps/store/config/
cp ${PATCH_PATH}/${PATCH_NAME}/resources/emm/modules/store.js ${EMM_PATH}/repository/deployment/server/jaggeryapps/emm/modules/

