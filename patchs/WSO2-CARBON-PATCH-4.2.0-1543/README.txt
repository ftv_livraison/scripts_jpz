Patch ID         : WSO2-CARBON-PATCH-4.2.0-1543
Applies To       : EMM 1.1.0
Associated JIRAS : https://wso2.org/jira/browse/EMM-807


DESCRIPTION
-------------
Some client may require to stop displaying store apps in the homepage slider for anonymous users. This patch provides a configurable feature to display/block 
app visibility for anonymous users.


INSTALLATION INSTRUCTIONS
---------------------------

(01) Shutdown the server, if you have already started.

(02) Copy the wso2carbon-version.txt file to <CARBON_SERVER>/bin.

(03) Copy the following files provided in resources folder of the patch to specified locations inside the server.

     (i) resources/store/modules/store.js ---> <emm_server>/repository/deployment/server/jaggeryapps/store/modules/

(04) Restart the server with :
	Linux/Unix : sh wso2server.sh
	Windows : wso2server.bat

