Patch ID         : WSO2-CARBON-PATCH-4.2.0-1621
Applies To       : EMM 1.1.0
Associated JIRAS : https://wso2.org/jira/browse/EMM-925


DESCRIPTION
-------------

When an iOS device is enrolled, in the ideal situation, the record that is put to DEVICE_PENDING table regarding this device should be removed at the phase of the enrollment success stage.

Issue: When there is a case where user of a secondary users store is enrolled, this record gets not removed from the table at the success stage.

INSTALLATION INSTRUCTIONS
---------------------------

(01) Shutdown the server, if you have already started.

(02) Copy the following files provided in resources folder of the patch to specified locations inside the server.

	 (i)	enrollsuccess.jag ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/ios_utils/

(03) Copy wso2carbon-version.txt file to <emm_server>/bin.

(04) Restart the server with one of the following commands.
     Linux/Unix :  sh wso2server.sh
     Windows    :  wso2server.bat

