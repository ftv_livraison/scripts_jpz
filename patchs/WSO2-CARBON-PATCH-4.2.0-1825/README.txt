Patch ID         : WSO2-CARBON-PATCH-4.2.0-1825
Applies To       : WSO2 API Manager 1.6.0, WSO2 API Manager 1.7.0, WSO2 API Manager 1.8.0, 
		   WSO2 API Manager 1.9.0, WSO2 API Manager 1.9.1, WSO2 App Factory 2.0.0, 
		   WSO2 App Manager 1.0.0, WSO2 App Manager 1.1.0, WSO2 Application Server 5.2.1, 
		   WSO2 Business Activity Monitor 2.4.1, WSO2 Business Activity Monitor 2.5.0, 
		   WSO2 Business Process Server 3.1.0, WSO2 Business Rules Server 2.1.0, 
		   WSO2 Complex Event Processor 3.1.0, WSO2 Data Services Server 3.1.1,
		   WSO2 Data Services Server 3.2.0, WSO2 Data Services Server 3.2.1,
		   WSO2 Data Services Server 3.2.2, WSO2 Elastic Load Balancer 2.1.1, 
		   WSO2 Enterprise Mobility Manager 1.0.0, WSO2 Enterprise Mobility Manager 1.1.0,
		   WSO2 Identity Server 4.6.0, WSO2 Identity Server 5.0.0, WSO2 Message Broker 2.2.0,
		   WSO2 Private PaaS 4.0.0, WSO2 Private PaaS 4.1.0, WSO2 Private PaaS 4.1.1,
		   WSO2 Storage Server 1.1.0, WSO2 Task Server 1.1.0

DESCRIPTION
-----------
This patch addresses a security vulnerability in LogViewer admin service. With this patch a user can retrieve only CARBON_LOGFILE appender log files inside carbon logs directory.


INSTALLATION INSTRUCTIONS
-------------------------

(i)  Shutdown the server, if you have already started.

(ii) Copy the patch1825 to  <CARBON_SERVER>/repository/components/patches/

(iii) Restart the server with :
       Linux/Unix :  sh wso2server.sh
       Windows    :  wso2server.bat

