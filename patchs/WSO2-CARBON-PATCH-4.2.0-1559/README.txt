Patch ID         : WSO2-CARBON-PATCH-4.2.0-1559
Applies To       : EMM 1.1.0
Associated JIRAS : https://wso2.org/jira/browse/EMM-810


DESCRIPTION
-------------
This patch has the fix for app policy monitoring. Now app package name is properly passed to the policy tested service and in enforce mode, it will be pushed back to the device. Also app blacklist feature updated with the same fix.


INSTALLATION INSTRUCTIONS
---------------------------

(01) Shutdown the server, if you have already started.

(02) Copy the wso2carbon-version.txt file to <CARBON_SERVER>/bin.

(03) Copy the following files provided in resources folder of the patch to specified locations inside the server.

     (i) 	resources/emm/modules/store.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/modules/
    
     (ii) 	resources/emm/modules/policy.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/modules/

     (iii) 	resources/emm/client_app/emm-agent-android.apk ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/client_app/

(04) Restart the server with :
	Linux/Unix : sh wso2server.sh
	Windows : wso2server.bat

