Patch ID         : WSO2-CARBON-PATCH-4.2.0-1826
Applies To       : WSO2 API Manager 1.5.0, WSO2 Application Server 5.2.0, WSO2 Business Process Server 3.2.0,
		   WSO2 Complex Event Processor 3.0.0, WSO2 Data Services Server 3.1.0, 
		   WSO2 Elastic Load Balancer 2.1.0, WSO2 Enterprise Service Bus 4.8.0, 
		   WSO2 Enterprise Service Bus 4.8.1, WSO2 Governance Registry 4.6.0, 
		   WSO2 Identity Server 4.5.0

DESCRIPTION
-----------
This patch addresses a security vulnerability in LogViewer admin service. With this patch a user can retrieve only CARBON_LOGFILE appender log files inside carbon logs directory.


INSTALLATION INSTRUCTIONS
-------------------------

(i)  Shutdown the server, if you have already started.

(ii) Copy the patch1826 to  <CARBON_SERVER>/repository/components/patches/

(iii) Restart the server with :
       Linux/Unix :  sh wso2server.sh
       Windows    :  wso2server.bat

