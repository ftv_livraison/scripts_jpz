Patch ID         : WSO2-CARBON-PATCH-4.2.0-0454
Applies To       : G-REG 4.6.0
Associated JIRA  : https://wso2.org/jira/browse/CARBON-14559
		   https://wso2.org/jira/browse/CARBON-14560
		   https://wso2.org/jira/browse/IDENTITY-1894
		   https://wso2.org/jira/browse/SS-269
		   https://wso2.org/jira/browse/IDENTITY-1843
		   https://wso2.org/jira/browse/CARBON-14600
		   https://wso2.org/jira/browse/IDENTITY-1844
		   https://wso2.org/jira/browse/IDENTITY-1927
		   https://wso2.org/jira/browse/BPS-458
		   https://wso2.org/jira/browse/CARBON-14651
		   https://wso2.org/jira/browse/IDENTITY-2019
		   https://wso2.org/jira/browse/CARBON-14651
		   https://wso2.org/jira/browse/IDENTITY-2044
		   https://wso2.org/jira/browse/CARBON-14704
		   https://wso2.org/jira/browse/CARBON-14728
		   https://wso2.org/jira/browse/CARBON-14729
		   https://wso2.org/jira/browse/IDENTITY-2056
		   https://wso2.org/jira/browse/CARBON-14747
		   https://wso2.org/jira/browse/IDENTITY-2053
		   https://wso2.org/jira/browse/CARBON-14750
		   https://wso2.org/jira/browse/CARBON-14751
		   https://wso2.org/jira/browse/CARBON-14752
		   https://wso2.org/jira/browse/CARBON-14753
		   https://wso2.org/jira/browse/CARBON-14754
		   https://wso2.org/jira/browse/CARBON-14761
		   https://wso2.org/jira/browse/IDENTITY-2177
		   https://wso2.org/jira/browse/CARBON-14776
		   https://wso2.org/jira/browse/IDENTITY-2204
		   https://wso2.org/jira/browse/CARBON-14769
		   https://wso2.org/jira/browse/IDENTITY-2385
		   https://wso2.org/jira/browse/IDENTITY-2016
		   https://wso2.org/jira/browse/IDENTITY-2483
		   https://wso2.org/jira/browse/IDENTITY-2281


DESCRIPTION
-----------
This patch contains all the changes applied to org.wso2.carbon.user.core-4.2.0 component from patch0002 to patch0008


INSTALLATION INSTRUCTIONS
-------------------------

(i) Shutdown the server, if you have already started.

(ii) Copy the wso2carbon-version.txt file to <CARBON_SERVER>/bin.

(iii) Copy the patch0454 to  <CARBON_SERVER>/repository/components/patches/

(iv) Replace mysql_cluster.sql file on <CARBON_SERVER>/dbscripts with file provided inside WSO2-CARBON-PATCH-4.2.0-0454/dbscripts

(v) Configure following property in <CARBON_SERVER>/repository/conf/user-mgt.xml under, Realm configuration as follows (This is an optional configuration)
	<Realm>
		<Configuration>
			<Property name="GetRoleListOfInternalUserSQL">{Custom SQL query}</Property>
		</Configuration>
	</Realm>
	

(vi) Restart the server with :
       Linux/Unix :  sh wso2server.sh
       Windows    :  wso2server.bat

