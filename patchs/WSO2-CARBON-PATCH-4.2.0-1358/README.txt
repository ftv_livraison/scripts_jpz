Patch ID         : WSO2-CARBON-PATCH-4.2.0-1358
Applies To       : EMM 1.1.0
Associated JIRAS : 

https://wso2.org/jira/browse/EMM-790
https://wso2.org/jira/browse/EMM-791
https://wso2.org/jira/browse/EMM-792
https://wso2.org/jira/browse/EMM-797 


DESCRIPTION
-------------
There were three main issues related to this patch.

1. Users doesn't load in emm/users/configuration page under users tab. 
2. Date format issue.
3. Secondary user store's users were not be able to login to the system/agent apps without appending the store
name as the prefix.


INSTALLATION INSTRUCTIONS
---------------------------

(01) Shutdown the server, if you have already started.

(02) Copy the following files provided in resources folder of the patch to specified locations inside the server.

     (i) 	oracle_db.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/sqlscripts/

     (ii) 	Then copy and paste the content of oracle_db.js file to db.js

	 (iii)  user.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/modules/

	 (iv)	device.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/modules/

	 (v)	enroll.jag ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/ios_utils/

	 (vi)	common.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/client/js/

(03) Copy wso2carbon-version.txt file to <emm_server>/bin.

(04) Restart the server with one of the following commands with -Dsetup option.
     Linux/Unix :  sh wso2server.sh
     Windows    :  wso2server.bat

