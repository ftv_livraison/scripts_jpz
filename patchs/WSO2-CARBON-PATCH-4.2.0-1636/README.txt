Patch ID         : WSO2-CARBON-PATCH-4.2.0-1636
Applies To       : All the products based on carbon 4.2.0


DESCRIPTION
-----------
This patch addresses the security vulnerability in java object deserialization.
https://issues.apache.org/jira/browse/COLLECTIONS-580


INSTALLATION INSTRUCTIONS
-------------------------

(i)  Shutdown the server, if you have already started.

(ii) Copy the wso2carbon-version.txt file to <CARBON_SERVER>/bin.

(iii) Copy the patch1636 to  <CARBON_SERVER>/repository/components/patches/

(iv) Restart the server with :
       Linux/Unix :  sh wso2server.sh
       Windows    :  wso2server.bat

