Patch ID         : WSO2-CARBON-PATCH-4.2.0-1695
Applies To       : EMM 1.1.0
Associated JIRAS : 

https://wso2.org/jira/browse/EMM-1160

DESCRIPTION
-------------

At the enrollment stage enrollment gets successful but after few days profile appear in red and asks to be updated. This is due to profile expiration. This patch provide a fix to hold a longer expiration duration. 


INSTALLATION INSTRUCTIONS
---------------------------

(01) Shutdown the server, if you have already started.

(02) Copy the following files provided in resources folder of the patch to specified locations inside the server.

org.wso2.carbon.emm.ios.core-1.1.0.jar ---> <emm_server>/repository/components/dropins/

(03) Restart the server with one of the following commands.
     Linux/Unix :  sh wso2server.sh
     Windows    :  wso2server.bat

