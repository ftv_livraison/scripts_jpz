Patch ID         : WSO2-CARBON-PATCH-4.2.0-1439
Applies To       : is-5.0.0
Associated JIRA  : https://wso2.org/jira/browse/IDENTITY-3408	
		   https://wso2.org/jira/browse/IDENTITY-3400	
		   https://wso2.org/jira/browse/IDENTITY-3437


DESCRIPTION
-----------
This patch fixes the above mentioned JIRA.


INSTALLATION INSTRUCTIONS
-------------------------

(i)  Shutdown the server, if you have already started.

(ii) Copy the wso2carbon-version.txt file to <CARBON_SERVER>/bin.

(iii) Copy the patch1439 to  <CARBON_SERVER>/repository/components/patches/

(iv) Restart the server with :
       Linux/Unix :  sh wso2server.sh
       Windows    :  wso2server.bat

