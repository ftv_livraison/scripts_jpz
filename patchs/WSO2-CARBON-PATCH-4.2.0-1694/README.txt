Patch ID         : WSO2-CARBON-PATCH-4.2.0-1694
Applies To       : EMM 1.1.0
Associated JIRAS : https://wso2.org/jira/browse/EMM-961


DESCRIPTION
-------------
This patch has the fix for the android agent app and the login feature of emm jaggery app ios login module. Now user password is properly URL encoded before sending the login request.

INSTALLATION INSTRUCTIONS
---------------------------

(01) Shutdown the server, if you have already started.

(02) Copy the following files provided in resources folder of the patch to specified locations inside the server.

     (i) resources/emm/modules/user.js ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/modules/
     (ii)resources/emm/ios_utils/deviceloginsign.jag ---> <emm_server>/repository/deployment/server/jaggeryapps/emm/ios_utils/

(03) Restart the server with :
	Linux/Unix : sh wso2server.sh
	Windows : wso2server.bat

(04) Apply the provided diff resources/emm-android-agent to the existing Android Agent source and rebuild the APK file and re-install on devices.

