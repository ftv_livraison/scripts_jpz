#!/bin/sh

PATCH_PATH=$1
EMM_PATH=$2
PATCH_FILE=$3
PATCH_NAME=`basename ${PATCH_FILE} .zip`

cd ${PATCH_PATH}

if [ ! -f "${PATCH_FILE}" ];
then
	echo "Patch file "${PATCH_FILE}" does not exist."
	exit 1;
fi

unzip ${PATCH_FILE}

cp ${PATCH_PATH}/${PATCH_NAME}/resources/oracle_db.js ${EMM_PATH}/repository/deployment/server/jaggeryapps/emm/sqlscripts/db.js
cp ${PATCH_PATH}/${PATCH_NAME}/resources/common.js ${EMM_PATH}/repository/deployment/server/jaggeryapps/emm/client/js/
cp ${PATCH_PATH}/${PATCH_NAME}/resources/device.js ${EMM_PATH}/repository/deployment/server/jaggeryapps/emm/modules/
cp ${PATCH_PATH}/${PATCH_NAME}/resources/user.js ${EMM_PATH}/repository/deployment/server/jaggeryapps/emm/modules/
cp ${PATCH_PATH}/${PATCH_NAME}/resources/enroll.jag  ${EMM_PATH}/repository/deployment/server/jaggeryapps/emm/ios_utils/

