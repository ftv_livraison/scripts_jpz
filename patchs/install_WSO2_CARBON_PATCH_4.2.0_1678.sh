#!/bin/sh

PATCH_PATH=$1
EMM_PATH=$2
PATCH_FILE=$3
PATCH_NAME=`basename ${PATCH_FILE} .zip`

cd ${PATCH_PATH}

if [ ! -f "${PATCH_FILE}" ];
then
	echo "Patch file "${PATCH_FILE}" does not exist."
	exit 1;
fi

unzip ${PATCH_FILE}

cp ${PATCH_PATH}/${PATCH_NAME}/resources/org.wso2.carbon.emm.ios.core-1.1.0.jar ${EMM_PATH}/repository/components/dropins/
cp ${PATCH_PATH}/${PATCH_NAME}/resources/org.wso2.carbon.emm.ios.payload-1.1.0.jar ${EMM_PATH}/repository/components/dropins/

