#!/bin/ksh
# @(#) install_WSO2-EMM.sh : procedure d'installation de WSO2-EMM
export FPATH=/appli/product/fonctions

init_envir $LINENO

# message de fin de traitement si exit
# # @(#) - Un message FIN_TRAITEMENT lors de l'exit
trap "analyse_log_file $LINENO" EXIT

init_log_etape $LINENO INSTALL-Java "Installation de Java dans $FTV_DIR_JDK"
if [ $DEBUG -eq 1 ] 
then
	msg_echo $LINENO "Etape non executee (DEBUG=$DEBUG)"
else
	{
	exec_CMD $LINENO cd $FTV_DIR_JDK
	exec_CMD $LINENO tar xvzf $FTV_DIR_PATCH/jdk-7u67-linux-x64.tar.gz
	} > $FIC_LOG 2>&1
fi
msg_echo $LINENO "Installation de JAVA terminee"
test_cr $LINENO $EXIT 1

init_log_etape $LINENO INSTALL-WSO2-EMM "Installation de WSO2-EMM dans $FTV_DIR_WSO2"
if [ $DEBUG -eq 1 ] 
then
	msg_echo $LINENO "Etape non executee (DEBUG=$DEBUG)"
else
	{
	exec_CMD $LINENO cd $FTV_DIR_WSO2
	exec_CMD $LINENO unzip -u $FTV_DIR_PATCH/wso2emm-1.1.0.zip 
	} > $FIC_LOG 2>&1
fi
msg_echo $LINENO "Installation de WSO2-EMM terminee"
test_cr $LINENO $EXIT 1

init_log_etape $LINENO INSTALL-oracle_driver "Installation du driver Oracle"
if [ $DEBUG -eq 1 ] 
then
	msg_echo $LINENO "Etape non executee (DEBUG=$DEBUG)"
else
	{
	exec_CMD $LINENO cp $FTV_DIR_PATCH/ojdbc6_1.0.0.jar $WSO2_HOME/repository/components/dropins/
	} > $FIC_LOG 2>&1
fi
msg_echo $LINENO "Installation du driver Oracle terminee"
test_cr $LINENO $EXIT 1

init_log_etape $LINENO INSTALL-mkdir_log "Creation des repertoires des logs"
if [ $DEBUG -eq 1 ] 
then
	msg_echo $LINENO "Etape non executee (DEBUG=$DEBUG)"
else
	{
	if [ -d $FTV_DIR_LOG ]
	then
		for REP in emm emm/logs emm/tmp
		do
			if [ -d $FTV_DIR_LOG/$REP ]
			then
				exec_CMD $LINENO mkdir $FTV_DIR_LOG/$REP
			else
				msg_echo $LINENO "$FTV_DIR_LOG/$REP existe deja"
			fi
		done
	else
		msg_echo $LINENO "Le repertoire $FTV_DIR_LOG n'existe pas"
		test_cr $LINENO 1
	fi
	} > $FIC_LOG 2>&1
fi
msg_echo $LINENO "Creation des repertoires des logs terminee"
test_cr $LINENO $EXIT 1

# Execution des scripts post installation
msg_echo $LINENO CONFIG-generate_CA "Execution des scripts de $FTV_DIR_POST_INSTALL"
for SCRIPT in $FTV_DIR_POST_INSTALL/[0-9][0-9]-*.sh
do
	PATCH_NAME=$(basename $SCRIPT | sed 's/\.sh//')
	init_log_etape $LINENO PATCH-$PATCH_NAME "Installation du patch $PATCH_NAME"
	if [ $DEBUG -eq 1 ] 
	then
		msg_echo $LINENO "Etape non executee (DEBUG=$DEBUG)"
	else
		ksh $SCRIPT > $FIC_LOG 2>&1
		test_cr $LINENO $?
	fi
	msg_echo $LINENO "Installation du patch $PATCH_NAME terminee"
	test_cr $LINENO $EXIT 1
done

# Generation de la CAtest_cr $LINENO $EXIT 1
init_log_etape $LINENO CONFIG-generate_CA "Generation de la Cerficate Authority"
if [ $DEBUG -eq 1 ] 
then
	msg_echo $LINENO "Etape non executee (DEBUG=$DEBUG)"
else
	{
	if [ ! -d $WSO2_GEN_CERT_DIR ]
	then
		exec_CMD $LINENO mkdir $WSO2_GEN_CERT_DIR
	fi
	
	exec_CMD $LINENO cd $WSO2_GEN_CERT_DIR
	exec_CMD $LINENO openssl genrsa -out ca_private.key 4096
	msg_echo $LINENO "openssl req -new -key ca_private.key -out ca.csr -subj \"/C=FR/ST=IDF/L=Paris/O=France Televisions/OU=IT/CN=$FTV_EMMCA_CERTALIAS/emailAddress=DeviceEnrollment@francetv.fr\""
	openssl req -new -key ca_private.key -out ca.csr -subj "/C=FR/ST=IDF/L=Paris/O=France Televisions/OU=IT/CN=$FTV_EMMCA_CERTALIAS/emailAddress=DeviceEnrollment@francetv.fr"
	test_cr $LINENO $?
	exec_CMD $LINENO openssl x509 -req -days 365 -in ca.csr -signkey ca_private.key -out ca.crt -extensions v3_ca -extfile $FTV_DIR_PATCH/openssl.cnf
	exec_CMD $LINENO openssl rsa -in ca_private.key -text > ca_private.pem
	exec_CMD $LINENO openssl x509 -in ca.crt -out ca_cert.pem

	# Generation de la RA
	exec_CMD $LINENO openssl genrsa -out ra_private.key 4096
	msg_echo $LINENO "openssl req -new -key ra_private.key -out ra.csr -subj \"/C=FR/ST=IDF/L=Paris/O=France Televisions/OU=IT/CN=$FTV_EMMRA_CERTALIAS/emailAddress=DeviceEnrollment@francetv.fr\""
	openssl req -new -key ra_private.key -out ra.csr -subj "/C=FR/ST=IDF/L=Paris/O=France Televisions/OU=IT/CN=$FTV_EMMRA_CERTALIAS/emailAddress=DeviceEnrollment@francetv.fr"
	test_cr $LINENO $?
	exec_CMD $LINENO openssl x509 -req -days 365 -in ra.csr -CA ca.crt -CAkey ca_private.key -set_serial 02 -out ra.crt -extensions v3_req -extfile $FTV_DIR_PATCH/openssl.cnf
	exec_CMD $LINENO openssl rsa -in ra_private.key -text > ra_private.pem
	exec_CMD $LINENO openssl x509 -in ra.crt -out ra_cert.pem

	# Generation du certificat SSL de communication HTTPS
	exec_CMD $LINENO openssl genrsa -out ia.key 4096
	msg_echo $LINENO "openssl req -new -key ia.key -out ia.csr -subj \"/C=FR/ST=IDF/L=Paris/O=France Televisions/OU=IT/CN=$FTV_STORE_URL/emailAddress=DeviceEnrollment@francetv.fr\""
	openssl req -new -key ia.key -out ia.csr -subj "/C=FR/ST=IDF/L=Paris/O=France Televisions/OU=IT/CN=$FTV_STORE_URL/emailAddress=DeviceEnrollment@francetv.fr"
	test_cr $LINENO $?
	exec_CMD $LINENO openssl x509 -req -days 730 -in ia.csr -CA ca_cert.pem -CAkey ca_private.pem -set_serial 044324343 -out ia.crt
	
	# Recuperation / deploiement des certificats et clefs privees
	exec_CMD $LINENO openssl rsa -in ia.key -out ia_nopwd.key
	
	# Export des cles dans les p12 distincts pour sauvegarde
	exec_CMD $LINENO openssl pkcs12 -export -out KEYSTORE.p12 -inkey ia.key -in ia.crt -CAfile ca_cert.pem -name $FTV_STORE_URL -passin pass:$FTV_CARBON_PASSWORD -passout pass:$FTV_CARBON_PASSWORD
	exec_CMD $LINENO openssl pkcs12 -export -out ca.p12 -inkey ca_private.pem -in ca_cert.pem -CAfile ssl.crt -name cacert -passin pass:$FTV_EMM_PASSWORD -passout pass:$FTV_EMM_PASSWORD
	exec_CMD $LINENO openssl pkcs12 -export -out ra.p12 -inkey ra_private.pem -in ra_cert.pem -CAfile ca_cert.pem -name racert -passin pass:$FTV_EMM_PASSWORD -passout pass:$FTV_EMM_PASSWORD
	
	# Export des clés dans les keystores java de WSO2 EMM
	msg_echo $LINENO "echo -n | openssl s_client -connect sppwmftvdc01.si.francetv.fr:636 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > certificat_ldap.pem"
	echo -n | openssl s_client -connect sppwmftvdc01.si.francetv.fr:636 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > certificat_ldap.pem
	test_cr $LINENO $?
	exec_CMD $LINENO openssl x509 -outform der -in certificat_ldap.pem -out certificat_ldap.der
	} > $FIC_LOG 2>&1
fi
msg_echo $LINENO "Generation de la Cerficate Authority terminee"
test_cr $LINENO $EXIT 1

###
init_log_etape $LINENO CONFIG-overwrite "Restaurer certificats"
if [ $DEBUG -eq 1 ] 
then
	msg_echo $LINENO "Etape non executee (DEBUG=$DEBUG)"
else
	{
	if [ -d $FTV_DIR_BACKUP_CERT ]
	then
		msg_echo $LINENO "Restaurer le contenu de $WSO2_GEN_CERT_DIR ..."
		exec_CMD $LINENO cp $FTV_DIR_BACKUP_CERT/gen_certificates/* $WSO2_GEN_CERT_DIR
	else
		msg_echo $LINENO "Le repertoire $FTV_DIR_BACKUP_CERT n'existe pas encore ..."
	fi
	} > $FIC_LOG 2>&1
fi
msg_echo $LINENO "Restaurer certificats terminee"
test_cr $LINENO $EXIT 1

init_log_etape $LINENO CONFIG-generate_JKS "Generation de la Cerficate Authority (fichiers .jks)"
if [ $DEBUG -eq 1 ] 
then
	msg_echo $LINENO "Etape non executee (DEBUG=$DEBUG)"
else
	{
	if [ ! -d $WSO2_GEN_CERT_DIR ]
	then
		exec_CMD $LINENO mkdir $WSO2_GEN_CERT_DIR
	fi
	
	exec_CMD $LINENO cd $WSO2_GEN_CERT_DIR
	
	exec_CMD $LINENO keytool -import -noprompt -alias SPPWMFTVDC01.si.francetv.fr -keystore $WSO2_HOME/repository/resources/security/client-truststore.jks -file certificat_ldap.der -storepass $FTV_CARBON_PASSWORD
	# Import de CA.p12/RA.p12 dans wso2emm.jks 
	exec_CMD $LINENO rm $WSO2_HOME/repository/resources/security/wso2emm.jks
	exec_CMD $LINENO keytool -importkeystore -srckeystore ra.p12 -srcstoretype pkcs12 -destkeystore $WSO2_HOME/repository/resources/security/wso2emm.jks -destalias $FTV_EMMRA_CERTALIAS -srcalias racert -srcstorepass $FTV_EMM_PASSWORD -deststorepass $FTV_EMM_PASSWORD
	exec_CMD $LINENO keytool -importkeystore -srckeystore ca.p12 -srcstoretype pkcs12 -destkeystore $WSO2_HOME/repository/resources/security/wso2emm.jks -destalias $FTV_EMMCA_CERTALIAS -srcalias cacert -srcstorepass $FTV_EMM_PASSWORD -deststorepass $FTV_EMM_PASSWORD
	
	# Import de KEYSTORE.p12/CA.p12/RA.p12 dans wso2carbon.jks
	exec_CMD $LINENO keytool -importkeystore -srckeystore KEYSTORE.p12 -srcstoretype pkcs12 -destkeystore $WSO2_HOME/repository/resources/security/wso2carbon.jks -destalias $FTV_STORE_URL -srcalias $FTV_STORE_URL -srcstorepass $FTV_CARBON_PASSWORD -deststorepass $FTV_CARBON_PASSWORD

	# Import de KEYSTORE.p12/CA.p12/RA.p12 dans client-truststore.jks :
	exec_CMD $LINENO keytool -importkeystore -srckeystore KEYSTORE.p12 -srcstoretype pkcs12 -destkeystore $WSO2_HOME/repository/resources/security/client-truststore.jks -destalias $FTV_STORE_URL -srcalias $FTV_STORE_URL -srcstorepass $FTV_CARBON_PASSWORD -deststorepass $FTV_CARBON_PASSWORD
	} > $FIC_LOG 2>&1
fi
msg_echo $LINENO "Generation de la Cerficate Authority  (fichiers .jks) terminee"
test_cr $LINENO $EXIT 1

init_log_etape $LINENO CONFIG-Generation-BKS "Generation du BKP pour l'agent Android"
if [ $DEBUG -eq 1 ] 
then
	msg_echo $LINENO "Etape non executee (DEBUG=$DEBUG)"
else
	{
	exec_CMD $LINENO cp $FTV_DIR_PATCH/bcprov-ext-jdk15on-1.46.jar $JAVA_HOME/jre/lib/ext/
	
	JAVA_RESSOURCE=org.bouncycastle.jce.provider.BouncyCastleProvider
	
	msg_echo $LINENO "Recherche de security.provider.*[0-9]=$JAVA_RESSOURCE dans le fichier $JAVA_HOME/jre/lib/security/java.security"
	CHECK_JAVA=$(grep -cE "security.provider.*[0-9]=$JAVA_RESSOURCE" $JAVA_HOME/jre/lib/security/java.security)
	if [ $CHECK_JAVA -eq 0 ]
	then
		msg_echo $LINENO "La ressource n'a pas ete trouvee"
		msg_echo $LINENO "Recherche du dernier numero de security.provider"
		LAST_NB_LINE=$(grep -E "security.provider.*[0-9]=" $JAVA_HOME/jre/lib/security/java.security | cut -d. -f3 | cut -d= -f1 | tail -1)
		[ "$LAST_NB_LINE" = "" ] && LAST_NB_LINE=0
		msg_echo $LINENO "Increment du dernier numero de security.provider ($LAST_NB_LINE + 1)"
		LAST_NB_LINE=$(( $LAST_NB_LINE + 1 ))
		test_cr $LINENO $?
		msg_echo $LINENO "Ajout de la ligne security.provider.$LAST_NB_LINE=$JAVA_RESSOURCE dans le fichier $JAVA_HOME/jre/lib/security/java.security"
		echo security.provider.$LAST_NB_LINE=$JAVA_RESSOURCE >> $JAVA_HOME/jre/lib/security/java.security
		test_cr $LINENO $?
	else
		msg_echo $LINENO "Modification de la configuration Java deja realisee."
	fi
	msg_echo $LINENO "Recherche de bcprov-ext-jdk15on-1.46.jar dans la variable CLASSPATH"
	if [ $(echo $CLASSPATH | grep -c bcprov-ext-jdk15on-1.46.jar) -eq 0 ]
	then
		msg_echo $LINENO "Ajout de $FTV_DIR_PATCH/bcprov-ext-jdk15on-1.46.jar a CLASSPATH"
		[ "$CLASSPATH" = "" ] && export CLASSPATH=$FTV_DIR_PATCH/bcprov-ext-jdk15on-1.46.jar || export CLASSPATH=$CLASSPATH:$FTV_DIR_PATCH/bcprov-ext-jdk15on-1.46.jar
		test_cr $LINENO $?
	else
		msg_echo $LINENO "$FTV_DIR_PATCH/bcprov-ext-jdk15on-1.46.jar est deja dans CLASSPATH"
	fi
	msg_echo $LINENO "CLASSPATH=$CLASSPATH"
	msg_echo $LINENO "Generation du certificat au format Android"
	exec_CMD $LINENO keytool -noprompt -importcert -v -trustcacerts -file $WSO2_GEN_CERT_DIR/ca_cert.pem -alias BKS -keystore $FTV_DIR_PATCH/emm_truststore.bks -provider org.bouncycastle.jce.provider.BouncyCastleProvider -providerpath "bcprov-jdk16-145.jar" -storetype BKS -storepass $FTV_CARBON_PASSWORD
	} > $FIC_LOG 2>&1
fi
msg_echo $LINENO "Generation du BKS pour l'agent Android terminee"
test_cr $LINENO $EXIT 1

init_log_etape $LINENO CONFIG-ReverseProxy "Initialisation du fichier de configuration Apache du Reverse Proxy"
if [ $DEBUG -eq 1 ] 
then
	msg_echo $LINENO "Etape non executee (DEBUG=$DEBUG)"
else
	ksh $SELF_DIR/CONFIG-ReverseProxy.sh > $FIC_LOG 2>&1
fi
msg_echo $LINENO "Creation de l'archive de configuration du Reverse Proxy terminee"
test_cr $LINENO $EXIT 1

init_log_etape $LINENO DEPLOY-ReverseProxy "Deploiement de l'archive de configuration sur le Reverse Proxy"
if [ $DEBUG -eq 1 ] 
then
	msg_echo $LINENO "Etape non executee (DEBUG=$DEBUG)"
else
	ksh $SELF_DIR/DEPLOY-ReverseProxy.sh 2>&1 | tee $FIC_LOG
fi
msg_echo $LINENO "Deploiement de l'archive de configuration sur le Reverse Proxy terminee"
test_cr $LINENO $EXIT 1

for INFO_AGENT in $WSO2_AGENTS 
do
	TYPE_AGENT=$(echo $INFO_AGENT | cut -d: -f1)
	FIC_AGENT=$(echo $INFO_AGENT | cut -d: -f2)
	init_log_etape $LINENO DEPLOY-Agent$TYPE_AGENT "Deploiement de l'agent $TYPE_AGENT"
	if [ $DEBUG -eq 1 ] 
	then
		msg_echo $LINENO "Etape non executee (DEBUG=$DEBUG)"
	else
		{
		if [ -f $FTV_DIR_AGENTS/$FIC_AGENT ]
		then
			exec_CMD $LINENO cp $FTV_DIR_AGENTS/$FIC_AGENT $WSO2_HOME/repository/deployment/server/jaggeryapps/emm/client_app/$FIC_AGENT
		else
			msg_echo $LINENO "Aucun agent $TYPE_AGENT a deployer ..."
		fi
		} > $FIC_LOG 2>&1
	fi
	msg_echo $LINENO "Deploiement de l'agent $TYPE_AGENT termine"
	test_cr $LINENO $EXIT 1
done

init_log_etape $LINENO RESTORE-upload "Restauration du repertoire upload du store si present"
if [ $DEBUG -eq 1 ] 
then
	msg_echo $LINENO "Etape non executee (DEBUG=$DEBUG)"
else
	{
	if [ -d $FTV_DIR_BACKUP_UPLOAD ]
	then
		if [ $(ls $FTV_DIR_BACKUP_UPLOAD | wc -l) -ne 0 ]
		then
			for F in $FTV_DIR_BACKUP_UPLOAD/*
			do
				msg_echo $LINENO "cp -p \"$F\" $WSO2_DIR_UPLOAD"
				cp -p "$F" $WSO2_DIR_UPLOAD
				test_cr $LINENO $?
			done
		else
			msg_echo $LINENO "Aucune sauvegarde de upload a restaurer"
			msg_echo $LINENO "Repertoire $FTV_DIR_BACKUP_UPLOAD vide."
		fi
	else
		msg_echo $LINENO "Aucune sauvegarde de upload a restaurer"
		msg_echo $LINENO "Repertoire $FTV_DIR_BACKUP_UPLOAD absent."
	fi
	} > $FIC_LOG 2>&1
fi
msg_echo $LINENO "Restauration du repertoire upload du store si present termine"
test_cr $LINENO $EXIT 1

init_log_etape $LINENO VALID-StartEMM "Demarrage de WSO2-EMM"
if [ $DEBUG -eq 1 ] 
then
	msg_echo $LINENO "Etape non executee (DEBUG=$DEBUG)"
else
	{
	exec_CMD $LINENO $WSO2_BIN/wso2server.sh start
	ps -ef | grep "$JAVA_HOME/bin/java" | grep -v grep
	} > $FIC_LOG 2>&1
fi
msg_echo $LINENO "Demarrage de WSO2-EMM termine"
test_cr $LINENO $EXIT 1

init_log_etape $LINENO BACKUP-config "Sauvegarde des certificats"
if [ $DEBUG -eq 1 ] 
then
	msg_echo $LINENO "Etape non executee (DEBUG=$DEBUG)"
else
	{
	if [ ! -d $FTV_DIR_BACKUP_CERT ]
	then
		msg_echo $LINENO "Creation de $FTV_DIR_BACKUP_CERT"
		exec_CMD $LINENO mkdir $FTV_DIR_BACKUP_CERT
	fi
	if [ ! -d $FTV_DIR_BACKUP_CERT/gen_certificates/ ]
	then
		msg_echo $LINENO "Sauvegarde du repertoire gen_certificates"
		exec_CMD $LINENO cp -R $WSO2_GEN_CERT_DIR $FTV_DIR_BACKUP_CERT/ 
		exec_CMD $LINENO cp $FTV_DIR_PATCH/emm_truststore.bks $FTV_DIR_BACKUP_CERT/ 
	fi
	} > $FIC_LOG 2>&1
fi
msg_echo $LINENO "Sauvegarde des certificats et agents terminee"
test_cr $LINENO $EXIT 1

exit 0