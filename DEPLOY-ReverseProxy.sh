#!/bin/ksh
# Script de deploiement de la configuration du ReverseProxy
# Il copie le fichier $FTV_DIR_PRODUCT/ReverseProxy.tar.gz sur le 
# reverse proxy puis le decompresse

export FPATH=/appli/product/fonctions

init_envir $LINENO
EXIT_NOW=1

echo
# msg_echo $LINENO " > > > >   Saisissez le mot de passe de diexpl sur $FTV_EMRP_HOST   < < < <"
echo 
exec_CMD $LINENO scp $FTV_DIR_PRODUCT/ReverseProxy.tar.gz diexpl@$FTV_EMRP_HOST:$FTV_DIR_PRODUCT/
# echo
# msg_echo $LINENO " > > > >   Saisissez le mot de passe de diexpl sur $FTV_EMRP_HOST   < < < <"
echo 
exec_CMD $LINENO ssh diexpl@$FTV_EMRP_HOST "cd $FTV_DIR_PRODUCT/;gzip -dc ReverseProxy.tar.gz | tar xvf -"