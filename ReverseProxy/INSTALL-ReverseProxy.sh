#!/bin/ksh
# Ce script a ete genere par CONFIG-ReverseProxy.sh sur spvlemm1
# Le  19/07/2016 a 13:43:31
# Fonction de test du code retour
test_cr() {
	[ $1 -eq 0 ] && echo \"OK\" || echo \"ERREUR\"
}
# Si le fichier httpd_EMRP existe dans /appli/product/ReverseProxy
[ -f /appli/product/ReverseProxy/httpd_EMRP ] && {
	# On le copie dans /etc/logrotate.d/
	echo cp /appli/product/ReverseProxy/httpd_EMRP /etc/logrotate.d/
	cp /appli/product/ReverseProxy/httpd_EMRP /etc/logrotate.d/
	test_cr $?
} || {
	# Sinon on informe
	echo /appli/product/ReverseProxy/httpd_EMRP non trouve
}
# Si le fichier France_televisions_2008_logo.png existe dans /appli/product/ReverseProxy
[ -f /appli/product/ReverseProxy/France_televisions_2008_logo.png ] && {
	# On le copie dans /var/www/html/
	echo cp /appli/product/ReverseProxy/France_televisions_2008_logo.png /var/www/html/
	cp /appli/product/ReverseProxy/France_televisions_2008_logo.png /var/www/html/
	test_cr $?
} || {
	# Sinon on informe
	echo /appli/product/ReverseProxy/France_televisions_2008_logo.png non trouve
}
# Si le fichier qr_code_production.png existe dans /appli/product/ReverseProxy
[ -f /appli/product/ReverseProxy/qr_code_production.png ] && {
	# On le copie dans /var/www/html/
	echo cp /appli/product/ReverseProxy/qr_code_production.png /var/www/html/
	cp /appli/product/ReverseProxy/qr_code_production.png /var/www/html/
	test_cr $?
} || {
	# Sinon on informe
	echo /appli/product/ReverseProxy/qr_code_production.png non trouve
}
# Si le fichier FTVchaines_fondblanc.png existe dans /appli/product/ReverseProxy
[ -f /appli/product/ReverseProxy/FTVchaines_fondblanc.png ] && {
	# On le copie dans /var/www/html/
	echo cp /appli/product/ReverseProxy/FTVchaines_fondblanc.png /var/www/html/
	cp /appli/product/ReverseProxy/FTVchaines_fondblanc.png /var/www/html/
	test_cr $?
} || {
	# Sinon on informe
	echo /appli/product/ReverseProxy/FTVchaines_fondblanc.png non trouve
}
# Si le fichier ROOTFTV.crt existe dans /appli/product/ReverseProxy
[ -f /appli/product/ReverseProxy/ROOTFTV.crt ] && {
	# On le copie dans /var/www/html/
	echo cp /appli/product/ReverseProxy/ROOTFTV.crt /var/www/html/
	cp /appli/product/ReverseProxy/ROOTFTV.crt /var/www/html/
	test_cr $?
} || {
	# Sinon on informe
	echo /appli/product/ReverseProxy/ROOTFTV.crt non trouve
}
# Si le fichier FTVStore-Manuel-Android.pdf existe dans /appli/product/ReverseProxy
[ -f /appli/product/ReverseProxy/FTVStore-Manuel-Android.pdf ] && {
	# On le copie dans /var/www/html/
	echo cp /appli/product/ReverseProxy/FTVStore-Manuel-Android.pdf /var/www/html/
	cp /appli/product/ReverseProxy/FTVStore-Manuel-Android.pdf /var/www/html/
	test_cr $?
} || {
	# Sinon on informe
	echo /appli/product/ReverseProxy/FTVStore-Manuel-Android.pdf non trouve
}
# Si le fichier FTVStore-Manuel-Apple.pdf existe dans /appli/product/ReverseProxy
[ -f /appli/product/ReverseProxy/FTVStore-Manuel-Apple.pdf ] && {
	# On le copie dans /var/www/html/
	echo cp /appli/product/ReverseProxy/FTVStore-Manuel-Apple.pdf /var/www/html/
	cp /appli/product/ReverseProxy/FTVStore-Manuel-Apple.pdf /var/www/html/
	test_cr $?
} || {
	# Sinon on informe
	echo /appli/product/ReverseProxy/FTVStore-Manuel-Apple.pdf non trouve
}
# Si le fichier wso2_emm.conf existe dans /appli/product/ReverseProxy
[ -f /appli/product/ReverseProxy/wso2_emm.conf ] && {
	# On le copie dans /etc/httpd/conf.d/
	echo cp /appli/product/ReverseProxy/wso2_emm.conf /etc/httpd/conf.d/
	cp /appli/product/ReverseProxy/wso2_emm.conf /etc/httpd/conf.d/
	test_cr $?
} || {
	# Sinon on informe
	echo /appli/product/ReverseProxy/wso2_emm.conf non trouve
}
# Si le fichier welcome.conf existe dans /appli/product/ReverseProxy
[ -f /appli/product/ReverseProxy/welcome.conf ] && {
	# On le copie dans /etc/httpd/conf.d/
	echo cp /appli/product/ReverseProxy/welcome.conf /etc/httpd/conf.d/
	cp /appli/product/ReverseProxy/welcome.conf /etc/httpd/conf.d/
	test_cr $?
} || {
	# Sinon on informe
	echo /appli/product/ReverseProxy/welcome.conf non trouve
}
# Si le fichier ca_cert.pem existe dans /appli/product/ReverseProxy
[ -f /appli/product/ReverseProxy/ca_cert.pem ] && {
	# On le copie dans /etc/httpd/conf.d/
	echo cp /appli/product/ReverseProxy/ca_cert.pem /etc/httpd/conf.d/
	cp /appli/product/ReverseProxy/ca_cert.pem /etc/httpd/conf.d/
	test_cr $?
} || {
	# Sinon on informe
	echo /appli/product/ReverseProxy/ca_cert.pem non trouve
}
# Si le fichier ia.crt existe dans /appli/product/ReverseProxy
[ -f /appli/product/ReverseProxy/ia.crt ] && {
	# On le copie dans /etc/httpd/conf.d/
	echo cp /appli/product/ReverseProxy/ia.crt /etc/httpd/conf.d/
	cp /appli/product/ReverseProxy/ia.crt /etc/httpd/conf.d/
	test_cr $?
} || {
	# Sinon on informe
	echo /appli/product/ReverseProxy/ia.crt non trouve
}
# Si le fichier ia_nopwd.key existe dans /appli/product/ReverseProxy
[ -f /appli/product/ReverseProxy/ia_nopwd.key ] && {
	# On le copie dans /etc/httpd/conf.d/
	echo cp /appli/product/ReverseProxy/ia_nopwd.key /etc/httpd/conf.d/
	cp /appli/product/ReverseProxy/ia_nopwd.key /etc/httpd/conf.d/
	test_cr $?
} || {
	# Sinon on informe
	echo /appli/product/ReverseProxy/ia_nopwd.key non trouve
}
# Recuperation de l etat de httpd_can_network_connect
httpd_can_network_connect=$(getsebool httpd_can_network_connect | cut -d' ' -f3)
# Si httpd_can_network_connect n est pas "on"
[ \"$httpd_can_network_connect\" != \"on\" ] && {
	echo setsebool -P httpd_can_network_connect on
	setsebool -P httpd_can_network_connect on
	test_cr $?
} || {
	# On ne le refait pas 2 fois (trop long)
	echo httpd_can_network_connect deja a on
}
echo apachectl restart
apachectl restart
test_cr $?
